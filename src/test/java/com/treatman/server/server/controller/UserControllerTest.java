package com.treatman.server.server.controller;

import com.treatman.server.controller.UserController;
import com.treatman.server.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@WebMvcTest(UserController.class)
class UserControllerTest {

    @MockBean
    private UserService userService;

    @Autowired
    private MockMvc mockMvc;

    @WithMockUser(value = "testAdminUser", authorities = "ADMIN")
    @Test
    void createUserValidRequestTest() throws Exception {
        String creationJson = "{\"username\": \"test\", \"password\":  \"123456\", \"firstName\": \"John\", \"lastName\": \"Doe\", \"email\": null}";
        mockMvc.perform(MockMvcRequestBuilders.post("/api/users/createUser")
                        .content(creationJson)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @WithMockUser(value = "testNormalUser")
    @Test
    void createUserValidRequestButWrongUserTest() throws Exception {
        String creationJson = "{\"username\": \"test\", \"password\":  \"123456\", \"firstName\": \"John\", \"lastName\": \"Doe\", \"email\": null}";
        mockMvc.perform(MockMvcRequestBuilders.post("/api/users/createUser")
                        .content(creationJson)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().is(HttpStatus.FORBIDDEN.value()));
    }

    @WithMockUser(value = "testAdminUser", authorities = "ADMIN")
    @Test
    void createUserWrongRequestButValidUserTest() throws Exception {
        String creationJson = "{\"username\": \"test\", \"password\":  \"123456\", \"firstName\": null, \"lastName\": \"Doe\", \"email\": null}";
        mockMvc.perform(MockMvcRequestBuilders.post("/api/users/createUser")
                        .content(creationJson)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().is(HttpStatus.BAD_REQUEST.value()));
    }

}
