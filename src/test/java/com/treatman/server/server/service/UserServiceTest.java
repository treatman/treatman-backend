package com.treatman.server.server.service;


import com.treatman.server.dto.request.UserCreateRequest;
import com.treatman.server.entities.User;
import com.treatman.server.exception.UserNotFoundException;
import com.treatman.server.repositories.UserRepository;
import com.treatman.server.security.PBKDF2Encoder;
import com.treatman.server.service.UserService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;

@ExtendWith(MockitoExtension.class)
class UserServiceTest {

    @Mock
    UserRepository userRepository;

    @Mock
    PBKDF2Encoder passwordEncoder;

    @InjectMocks
    UserService userService;

    @Test
    void findByUsernameTest() throws UserNotFoundException {
        User testUser = getTestUser();
        Mockito.when(userRepository.findByUsernameEquals("test")).thenReturn(Optional.of(testUser));

        User test = userService.findByUsername("test");
        assertNotNull(test);
        assertEquals( "test", test.getUsername());
    }

    @Test
    void findByUsernameUserNotFoundTest() {
        Mockito.when(userRepository.findByUsernameEquals("notExisting")).thenReturn(Optional.empty());

        Assertions.assertThrows(UserNotFoundException.class, () -> {
            userService.findByUsername("notExisting");
        });
    }

    @Test
    void createNewUserSuccess() {
        User createdUser = getTestUser();
        Mockito.when(passwordEncoder.encode("123456")).thenReturn("123456");
        Mockito.when(userRepository.save(any())).thenReturn(createdUser);

        UserCreateRequest userCreateRequest = new UserCreateRequest();
        userCreateRequest.setUsername("test");
        userCreateRequest.setEmail("test@test.de");
        userCreateRequest.setFirstName("John");
        userCreateRequest.setLastName("Doe");
        userCreateRequest.setPassword("123456");

        User user = userService.createUser(userCreateRequest);

        Mockito.verify(passwordEncoder, times(1)).encode("123456");
        Mockito.verify(userRepository, times(1)).save(any());
        assertNotNull(user);
        assertEquals(1L, user.getId());
    }

    private User getTestUser() {
        User testUser = new User();
        testUser.setId(1L);
        testUser.setUsername("test");
        testUser.setPassword("12345676234");
        testUser.setEmail("test@test.de");
        testUser.setFirstName("John");
        testUser.setLastName("Doe");

        return testUser;
    }

}
