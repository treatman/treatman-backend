package com.treatman.server.server.security;

import com.treatman.server.security.PBKDF2Encoder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.test.util.ReflectionTestUtils;

import static org.junit.jupiter.api.Assertions.*;

class PBKDF2EncoderTest {

    private PBKDF2Encoder passwordEncoder;

    @BeforeEach
    public void setup() {
        this.passwordEncoder = new PBKDF2Encoder();
    }

    @Test
    void testEncode() {
        ReflectionTestUtils.setField(passwordEncoder, "secret", "mysecret");
        ReflectionTestUtils.setField(passwordEncoder, "iteration", 33);
        ReflectionTestUtils.setField(passwordEncoder, "keylength", 256);
        String encode = passwordEncoder.encode("123456");
        assertNotNull(encode);
        assertNotEquals("123456", encode);
    }

    @Test
    void testMatches() {
        ReflectionTestUtils.setField(passwordEncoder, "secret", "mysecret");
        ReflectionTestUtils.setField(passwordEncoder, "iteration", 33);
        ReflectionTestUtils.setField(passwordEncoder, "keylength", 256);
        String encode = passwordEncoder.encode("123456");
        assertNotNull(encode);

        assertTrue(passwordEncoder.matches("123456", encode));
    }

}
