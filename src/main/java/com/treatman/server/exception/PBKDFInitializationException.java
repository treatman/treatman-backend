package com.treatman.server.exception;

public class PBKDFInitializationException extends Exception {
    public PBKDFInitializationException(Exception ex) {
        super(ex);
    }
}
