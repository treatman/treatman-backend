package com.treatman.server.service;

import com.treatman.server.dto.request.UserCreateRequest;
import com.treatman.server.entities.User;
import com.treatman.server.exception.UserNotFoundException;
import com.treatman.server.repositories.UserRepository;
import com.treatman.server.security.PBKDF2Encoder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Slf4j
public class UserService {

    private final UserRepository userRepository;
    private final PBKDF2Encoder passwordEncoder;

    @Autowired
    public UserService(UserRepository userRepository, PBKDF2Encoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    public User findByUsername(String username) throws UserNotFoundException {
        log.debug("Searching for user (username={})", username);
        return userRepository.findByUsernameEquals(username).orElseThrow(UserNotFoundException::new);
    }

    @Transactional
    public User createUser(UserCreateRequest request) {

        User userToCreate = new User();
        userToCreate.setFirstName(request.getFirstName());
        userToCreate.setLastName(request.getLastName());
        userToCreate.setUsername(request.getUsername());
        userToCreate.setPassword(passwordEncoder.encode(request.getPassword()));
        userToCreate.setEmail(request.getEmail());
        userToCreate.setEnabled(true);

        return userRepository.save(userToCreate);
    }
}
