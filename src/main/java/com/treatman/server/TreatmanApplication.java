package com.treatman.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "com.treatman.server")
public class TreatmanApplication {

    public static void main(String[] args) {
        SpringApplication.run(TreatmanApplication.class, args);
    }

}
