package com.treatman.server.controller;

import com.treatman.server.dto.request.AuthRequest;
import com.treatman.server.dto.response.AuthResponse;
import com.treatman.server.entities.User;
import com.treatman.server.exception.UserNotFoundException;
import com.treatman.server.security.JWTUtil;
import com.treatman.server.security.PBKDF2Encoder;
import com.treatman.server.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class AuthenticationController {

    private final JWTUtil jwtUtil;
    private final UserService userService;
    private final PBKDF2Encoder passwordEncoder;

    @Autowired
    public AuthenticationController(JWTUtil jwtUtil, UserService userService, PBKDF2Encoder passwordEncoder) {
        this.jwtUtil = jwtUtil;
        this.userService = userService;
        this.passwordEncoder = passwordEncoder;
    }

    @PostMapping("/api/login")
    public ResponseEntity<AuthResponse> login(@RequestBody AuthRequest authRequest) {

        try {
            User u = userService.findByUsername(authRequest.getUsername());
            if (passwordEncoder.encode(authRequest.getPassword()).equals(u.getPassword())) {
                AuthResponse response = new AuthResponse();
                String token = jwtUtil.generateToken(u);
                response.setMessage("Successfully logged in");
                response.setToken(token);
                return ResponseEntity.ok(response);
            } else {
                return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
            }
        } catch (UserNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }

    }

}
