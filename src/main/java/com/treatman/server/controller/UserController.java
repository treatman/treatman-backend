package com.treatman.server.controller;

import com.treatman.server.dto.request.UserCreateRequest;
import com.treatman.server.dto.response.GeneralOperationResponse;
import com.treatman.server.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@Slf4j
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @PostMapping("/api/users/createUser")
    public ResponseEntity<GeneralOperationResponse> createUser(@Valid @RequestBody UserCreateRequest request) {

        userService.createUser(request);

        GeneralOperationResponse response = new GeneralOperationResponse();
        response.setMessage("Successfully created new User!");
        response.setSuccess(true);
        return ResponseEntity.ok(response);
    }

}
