package com.treatman.server.dto.response;

import lombok.Data;

@Data
public class AuthResponse {

    private String message;
    private String token;

}
