package com.treatman.server.dto.response;

import lombok.Data;

@Data
public class GeneralOperationResponse {
    private String message;
    private Boolean success;
}
