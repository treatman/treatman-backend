package com.treatman.server.dto.request;

import lombok.Data;
import lombok.ToString;

import javax.validation.constraints.NotEmpty;

@Data
public class UserCreateRequest {

    @NotEmpty
    private String username;

    @ToString.Exclude
    @NotEmpty
    private String password;

    @NotEmpty
    private String firstName;

    @NotEmpty
    private String lastName;

    private String email;

}
