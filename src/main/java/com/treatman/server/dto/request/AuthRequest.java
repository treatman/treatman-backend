package com.treatman.server.dto.request;

import lombok.Data;
import lombok.ToString;

@Data
public class AuthRequest {

    private String username;

    @ToString.Exclude
    private String password;

}
